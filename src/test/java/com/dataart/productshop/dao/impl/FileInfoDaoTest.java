package com.dataart.productshop.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.dataart.productshop.dao.FileInfoDao;
import com.dataart.productshop.dao.TypeDao;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.model.Type;

@TransactionConfiguration(defaultRollback = true)
public class FileInfoDaoTest extends BaseDaoTestCase{

	@Autowired
	FileInfoDao fileInfoDao;
	@Autowired
	TypeDao typeDao;
	@Autowired
	Log log;

	@Before
	public void setUp(){
		for (FileInfo fileInfo : fileInfoDao.getAll()) {
			fileInfoDao.delete(fileInfo.getId());
		}
		for (Type type: typeDao.getAll()) {
			typeDao.delete(type.getId());
		}
	}

	@After
	public void removeAllData(){
		for (FileInfo fileInfo : fileInfoDao.getAll()) {
			fileInfoDao.delete(fileInfo.getId());
		}
		for (Type type: typeDao.getAll()) {
			typeDao.delete(type.getId());
		}
	}


	@Test
	public void crudFileInfo(){
		String name = "name";
		String description = "description";
		String filePath = "filePath";
		String typeName = "typeName";
		String mainTitlePath = "mainTitlePath";
		String icon = "icon";
		String hash = "1234567897654345676543";
		Long typeId;
		Long fileInfoId;

		assertNotNull(fileInfoDao);
		Type type = new Type();
		type.setName(typeName);
		typeDao.save(type);
		typeId = type.getId();
		assertNotNull(typeId);

		type = typeDao.getById(typeId);
		
		FileInfo testFileInfo = new FileInfo();
		testFileInfo.setCreatedDate(new Date());
		testFileInfo.setName(name);
		testFileInfo.setDescription(description);
		testFileInfo.setFilePath(filePath);
		testFileInfo.setIcon(icon);
		testFileInfo.setHash(hash);
		testFileInfo.setType(type);
		
		fileInfoDao.save(testFileInfo);
		
		assertNotNull(testFileInfo.getId());
		fileInfoId = testFileInfo.getId();

		testFileInfo = fileInfoDao.getById(fileInfoId);
		assertNotNull(testFileInfo);
		assertEquals("retrieved FileInfo.name not equals to initial", name, testFileInfo.getName());
		assertEquals("retrieved FileInfo.description not equals to initial",
				description, testFileInfo.getDescription());
		assertEquals("retrieved FileInfo.filePath not equals to initial"
				, filePath, testFileInfo.getFilePath());
		assertEquals("retrieved FileInfo.icon not equals to initial", icon, testFileInfo.getIcon());
		assertEquals("retrieved FileInfo.hash not equals to initial", hash, testFileInfo.getHash());
		assertEquals("retrieved FileInfo.type not equals to initial", type, testFileInfo.getType());
		assertNull("retrieved FileInfo.mainTitle not equals to initial", testFileInfo.getMainTitle());

		testFileInfo.setMainTitle(mainTitlePath);

		testFileInfo = fileInfoDao.getById(fileInfoId);
		assertNotNull(testFileInfo);
		assertEquals("retrieved second time FileInfo.name not equals to initial", name, testFileInfo.getName());
		assertEquals("retrieved second time FileInfo.description not equals to initial",
				description, testFileInfo.getDescription());
		assertEquals("retrieved second time FileInfo.filePath not equals to initial"
				, filePath, testFileInfo.getFilePath());
		assertEquals("retrieved second time FileInfo.icon not equals to initial",
				icon, testFileInfo.getIcon());
		assertEquals("retrieved second time FileInfo.hash not equals to initial",
				hash, testFileInfo.getHash());
		assertEquals("retrieved second time FileInfo.type not equals to initial",
				type, testFileInfo.getType());
		assertEquals("retrieved second time FileInfo.mainTitle not equals to initial",
				mainTitlePath, testFileInfo.getMainTitle());

		fileInfoDao.delete(fileInfoId);
		typeDao.delete(typeId);

		assertNull(fileInfoDao.getById(fileInfoId));
		assertNull(typeDao.getById(typeId));
	}

	@Test
	public void shouldRetrieveFileInfoByDownloadsAndThenCheckOrdering(){
		// I know that it could be implemented via lists or arrays;
		// but I think this style more clearly
		//here is less camelcase than always
		final int sizeOfFinallyRetrievedType = 4;
		final String someName = "someName";
		final String somePath = "somePath";

		long numbers_of_downloads_1 = 1111111;
		long numbers_of_downloads_2 = 111111;
		long numbers_of_downloads_3 = 11111;
		long numbers_of_downloads_4 = 1111;
		long numbers_of_downloads_5 = 111;
		long numbers_of_downloads_6 = 11;
		long numbers_of_downloads_7 = 1;

		Type type = new Type();
		type.setName(someName);
		typeDao.save(type);

		FileInfo testFileInfo_1 = new FileInfo();
		FileInfo testFileInfo_2 = new FileInfo();
		FileInfo testFileInfo_3 = new FileInfo();
		FileInfo testFileInfo_4 = new FileInfo();
		FileInfo testFileInfo_5 = new FileInfo();
		FileInfo testFileInfo_6 = new FileInfo();
		FileInfo testFileInfo_7 = new FileInfo();

		setNotNullParams(testFileInfo_1, someName, somePath, type, numbers_of_downloads_1);
		setNotNullParams(testFileInfo_2, someName, somePath, type, numbers_of_downloads_2);
		setNotNullParams(testFileInfo_3, someName, somePath, type, numbers_of_downloads_3);
		setNotNullParams(testFileInfo_4, someName, somePath, type, numbers_of_downloads_4);
		setNotNullParams(testFileInfo_5, someName, somePath, type, numbers_of_downloads_5);
		setNotNullParams(testFileInfo_6, someName, somePath, type, numbers_of_downloads_6);
		setNotNullParams(testFileInfo_7, someName, somePath, type, numbers_of_downloads_7);

		//pseudo-random saving
		fileInfoDao.save(testFileInfo_4);
		fileInfoDao.save(testFileInfo_3);
		fileInfoDao.save(testFileInfo_5);
		fileInfoDao.save(testFileInfo_1);
		fileInfoDao.save(testFileInfo_2);
		fileInfoDao.save(testFileInfo_7);
		fileInfoDao.save(testFileInfo_6);

		List<FileInfo> retrievedList = fileInfoDao.getByDownloads(2,4);
		assertNotNull("list of FileInfo not retrieved", retrievedList);
		assertEquals("list of FileInfo not fully retrieved : " + retrievedList, sizeOfFinallyRetrievedType,
				retrievedList.size());
		assertEquals("first fileInfo not equal", testFileInfo_3, retrievedList.get(0));
		assertEquals("second fileInfo not equal",testFileInfo_4, retrievedList.get(1));
		assertEquals("3rd fileInfo not equal",testFileInfo_5, retrievedList.get(2));
		assertEquals("4th fileInfo not equal",testFileInfo_6, retrievedList.get(3));
	}

	@Test
	public void shouldStoreFileInfoThenRetrieveByTypeId(){
		int sizeOfFinallyRetrievedType = 3;
		final String typeName_1 = "typeName1";
		final String typeName_2 = "typeName2";
		final String someName = "someName";
		final String somePath = "somePath";
		Type type_1 = new Type();
		Type type_2 = new Type();
		type_1.setName(typeName_1);
		type_2.setName(typeName_2);
		typeDao.save(type_1);
		typeDao.save(type_2);

		long numbers_of_downloads_1 = 1111111;
		long numbers_of_downloads_2 = 111111;
		long numbers_of_downloads_3 = 11111;
		long numbers_of_downloads_4 = 1111;
		long numbers_of_downloads_5 = 111;
		long numbers_of_downloads_6 = 11;
		long numbers_of_downloads_7 = 1;

		assertNotNull("type_1 stored not correctly", type_1.getId());
		assertNotNull("type_1 stored not correctly", type_2.getId());

		FileInfo testFileInfo_1 = new FileInfo();
		FileInfo testFileInfo_2 = new FileInfo();
		FileInfo testFileInfo_3 = new FileInfo();
		FileInfo testFileInfo_4 = new FileInfo();
		FileInfo testFileInfo_5 = new FileInfo();
		FileInfo testFileInfo_6 = new FileInfo();
		FileInfo testFileInfo_7 = new FileInfo();

		setNotNullParams(testFileInfo_1, someName, somePath, type_2, numbers_of_downloads_1);
		setNotNullParams(testFileInfo_2, someName, somePath, type_2, numbers_of_downloads_2);
		setNotNullParams(testFileInfo_3, someName, somePath, type_2, numbers_of_downloads_3);
		setNotNullParams(testFileInfo_4, someName, somePath, type_2, numbers_of_downloads_4);
		setNotNullParams(testFileInfo_5, someName, somePath, type_1, numbers_of_downloads_5);
		setNotNullParams(testFileInfo_6, someName, somePath, type_1, numbers_of_downloads_6);
		setNotNullParams(testFileInfo_7, someName, somePath, type_1, numbers_of_downloads_7);

		//pseudo-random saving
		fileInfoDao.save(testFileInfo_4);
		fileInfoDao.save(testFileInfo_3);
		fileInfoDao.save(testFileInfo_5);
		fileInfoDao.save(testFileInfo_1);
		fileInfoDao.save(testFileInfo_2);
		fileInfoDao.save(testFileInfo_7);
		fileInfoDao.save(testFileInfo_6);

		List<FileInfo> retrievedList = fileInfoDao.getFileInfoByTypeId(type_1.getId());
		assertNotNull("list of FileInfo not retrieved", retrievedList);
		assertEquals("list of FileInfo not fully retrieved " + retrievedList, sizeOfFinallyRetrievedType,
				retrievedList.size());
		assertEquals("first fileInfo not equal", testFileInfo_5, retrievedList.get(0));
		assertEquals("first fileInfo not equal", testFileInfo_6, retrievedList.get(1));
		assertEquals("first fileInfo not equal", testFileInfo_7, retrievedList.get(2));
	}

	@Test
	public void shouldStoreThenRetrieveByHash(){
		int sizeOfFinallyRetrievedType = 3;
		final String hash1 = "23a6f6d6b3a334bfd";
		final String hash2 = "23a6fdfdfdfdfdfdf6d6b3a334bfd";
		final String typeName = "typeName1";
		final String typeName_2 = "typeName2";
		final String someName = "someName";
		final String somePath = "somePath";
		Type type = new Type();
		type.setName(typeName);
		typeDao.save(type);

		assertNotNull("type_1 stored not correctly", type.getId());
		FileInfo testFileInfo_1 = new FileInfo();
		FileInfo testFileInfo_2 = new FileInfo();

		setNotNullParams(testFileInfo_1, someName, somePath, type, 0L);
		setNotNullParams(testFileInfo_2, someName, somePath, type, 0L);
		testFileInfo_1.setHash(hash1);
		testFileInfo_2.setHash(hash2);

		//pseudo-random saving
		fileInfoDao.save(testFileInfo_1);
		fileInfoDao.save(testFileInfo_2);

		FileInfo retrievedFileInfo= fileInfoDao.getByHash(hash1);
		assertNotNull("FileInfo not retrieved", retrievedFileInfo);
		assertEquals("first fileInfo not equal", testFileInfo_1, retrievedFileInfo);
	}

	private void setNotNullParams(FileInfo fi, String name, String path, Type type, Long downloads){
		fi.setName(name);
		fi.setFilePath(path);
		fi.setType(type);
		fi.setDownloads(downloads);
	}
}
