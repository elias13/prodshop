package com.dataart.productshop.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.dataart.productshop.dao.TypeDao;
import com.dataart.productshop.model.Type;

@TransactionConfiguration(defaultRollback = true)
public class TypeDaoTest extends BaseDaoTestCase{
	
	@Autowired
	TypeDao typeDao;
	
	@Test
	public void crudType(){
		String typeName = "someName";
		String newName = "newName";

		Type testType = new Type();
		Long typeId;
		
		testType.setName(typeName);
		typeDao.save(testType);

		assertNotNull("wrong store type => doesn't give an id",testType.getId());
		typeId = testType.getId();

		testType = typeDao.getById(typeId);
		assertNotNull("retrieving type failed", testType);
		assertEquals("retrieved Type.name not equals to initial", typeName, testType.getName());

		testType.setName(newName);
		typeDao.save(testType);

		testType = typeDao.getById(typeId);
		assertNotNull("retrieving type failed", testType);
		assertEquals("retrieved second time Type.name not equals to initial", newName, testType.getName());

		typeDao.delete(typeId);
		testType = typeDao.getById(typeId);
		assertNull("failed on deleting Type", testType);
	}

	@Test
	public void shouldStoreTypeThenRetrieveItByName(){
		String typeName = "someName";

		Type testType = new Type();
		testType.setName(typeName);
		typeDao.save(testType);

		testType = typeDao.getByName(typeName);
		assertNotNull("retrieving type failed", testType);
		assertEquals("retrieved Type.name not equals to initial", typeName, testType.getName());
	}
}
