package com.dataart.productshop.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/spring/application-context.xml")
public class BaseDaoTestCase extends AbstractTransactionalJUnit4SpringContextTests {
	public static final Long ID_HAVE_TO_BE = 1L;
	public static final int MIN_PICTURE_NUMBER = 4;
	
	@Test
	public void forMavenShutUpTest(){
		
	}
}
