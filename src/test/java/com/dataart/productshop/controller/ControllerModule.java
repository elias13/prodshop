package com.dataart.productshop.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dataart.productshop.Constants;


@ContextConfiguration(locations = {"file:src/test/resources/spring/application-context.xml"})
public class ControllerModule{

    @Value(Constants.DEFAULT_IMAGE_URL)
    String defaultImageUrl;

    @Test
    public void testExistsDefaultPicture(){
    }
}
