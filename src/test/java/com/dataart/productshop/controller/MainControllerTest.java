package com.dataart.productshop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.dataart.productshop.Constants;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.model.Type;
import com.dataart.productshop.service.FileInfoManager;
import com.dataart.productshop.service.TypeManager;

@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest {

	private static final String FIST_TEST_TYPE_NAME = "someTestTypeTestNameAndItIsVeryLong";
	private static final String SECOND_TEST_TYPE_NAME = "anotherTypeNameThatShorter";
	private static final Long ACCESSIBLE_ID = 23440813L;
	private static final Long NON_ACCESSIBLE_ID = 23440814L;

	private MainController mainController;
	private List<Type> types = new ArrayList<Type>();
	private List<FileInfo> testPopularFileInfo;
	private List<FileInfo> testFileInfoReturnedByGetTypeByIdMethod;

	@Mock
	TypeManager typeManager;

	@Mock
	FileInfoManager fileInfoManager;

	@Before
	public void addSetup(){
		Type firstTestType = new Type();
		firstTestType.setName(FIST_TEST_TYPE_NAME);
		Type secondTestType = new Type();
		secondTestType.setName(SECOND_TEST_TYPE_NAME);

		types.add(firstTestType);
		types.add(secondTestType);

		testPopularFileInfo = new ArrayList<FileInfo>();
		testFileInfoReturnedByGetTypeByIdMethod = new ArrayList<FileInfo>();

		Collections.fill(testPopularFileInfo,new FileInfo());
		Collections.fill(testFileInfoReturnedByGetTypeByIdMethod,new FileInfo());


		when(typeManager.getAll()).thenReturn(types);
		when(fileInfoManager.getFullFive()).thenReturn(testPopularFileInfo);
		when(fileInfoManager.getFileInfoByTypeId(ACCESSIBLE_ID)).thenReturn(testFileInfoReturnedByGetTypeByIdMethod);
		when(fileInfoManager.getFileInfoByTypeId(NON_ACCESSIBLE_ID)).thenReturn(null);

		mainController = new MainController();
		mainController.setTypeManager(typeManager);
		mainController.setFileInfoManager(fileInfoManager);

	}

	@Test
	public void methodInitHomePageShouldInitHomePage(){
		ModelAndView mav = mainController.initHomePage();

		assertNotNull("ModelAndViewResult is null", mav);
		assertEquals("wrong view name was init in MAV-result-object", MainController.HOME_PAGE, mav.getViewName());
		assertEquals("wrong types was init in MAV-result-object", types, mav.getModel().get(MainController.CATEGORIES));
		assertEquals("wrong fileInfos was init in MAV-result-object", testPopularFileInfo, mav.getModel().get(MainController.FILE_INFO_POPULAR_FILES));
		assertEquals("wrong active header was init in MAV-result-object", MainController.HOME_PAGE, mav.getModel().get(Constants.ACTIVE_HEADER));
	}

	@Test
	public void methodGetHomeAsynchShouldReturnInitHomePageData(){
		ModelAndView mav = mainController.getHomeAsynch();

		assertNotNull("ModelAndViewResult is null", mav);
		assertEquals("wrong view name was init in MAV-result-object", MainController.MAIN_CONTENT_VIEW, mav.getViewName());
		assertEquals("wrong types was init in MAV-result-object", types, mav.getModel().get(MainController.CATEGORIES));
		assertEquals("wrong fileInfos was init in MAV-result-object", testPopularFileInfo, mav.getModel().get(MainController.FILE_INFO_POPULAR_FILES));
		assertEquals("wrong active header was init in MAV-result-object", MainController.HOME_PAGE, mav.getModel().get(Constants.ACTIVE_HEADER));
	}

	@Test
	public void shouldRunCorrectlyRunMethodLoadFileInfoByCategory(){
		ModelAndView mav = mainController.loadFileInfoByCategory(ACCESSIBLE_ID);

		assertNotNull("ModelAndViewResult is null", mav);
		assertEquals("wrong view name was init in MAV-result-object", MainController.FILE_INFO_LIST_VIEW, mav.getViewName());
		assertEquals("wrong fileInfos was init in MAV-result-object", testFileInfoReturnedByGetTypeByIdMethod, mav.getModel().get(MainController.FILE_INFOS));
	}

	@Test
	public void shouldRunCorrectlyRunMethodLoadFileInfoByCategoryButReturnEmptyCollectionInModel(){
		ModelAndView mav = mainController.loadFileInfoByCategory(NON_ACCESSIBLE_ID);

		assertNotNull("ModelAndViewResult is null", mav);
		assertEquals("wrong view name was init in MAV-result-object", MainController.FILE_INFO_LIST_VIEW, mav.getViewName());
		assertNull("wrong fileInfos was init in MAV-result-object", mav.getModel().get(MainController.FILE_INFOS));
	}

	@Test
	public void methodCallGetPopularGridTestsResultMAVContent(){
		ModelAndView mav = mainController.getPopularGrid();

		assertNotNull("ModelAndViewResult is null", mav);
		assertEquals("wrong view name was init in MAV-result-object", MainController.POPULAR_GRID_VIEW, mav.getViewName());
		assertEquals("wrong fileInfos was init in MAV-result-object", testPopularFileInfo, mav.getModel().get(MainController.FILE_INFO_POPULAR_FILES));

	}




}
