package com.dataart.productshop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.dataart.productshop.file.utils.PictureUtils;
import com.dataart.productshop.file.utils.SaveFileUtils;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.model.Type;
import com.dataart.productshop.service.FileInfoManager;
import com.dataart.productshop.service.TypeManager;
import com.dataart.productshop.submodel.UploadFileItem;

@RunWith(MockitoJUnitRunner.class)
public class UploadControllerTest {
	private static final String OCCUPIED_FILE_INFO_NAME = "somrFileInfoNameWhichAlreadyBeenStored";
	private static final String FREE_FILE_INFO_NAME = "someStringThatHasNothingToDoWith";
	private static final String PICTURE_PATH = "somePathToImaginePicture";
	private static final int IMAGE_SIZE = 183382;
	private static final int IMAGE_ACC = 12;
	private static final String FOLDER_PATH = "imagineFolderPathToImaginationLand";

	private static final String NAME = "theName";
	private static final String DESC= "theDecsr";
	private static final String FILENAME = "theFileName.zip";
	private static final String HASH= "theHASH";
	private static final String TYPE_ID_STRING = "2";
	private static final long TYPE_ID = 2L;

	private UploadController uploadControllerPartialMocked;
	private List<String> testListOfPicturePathes;
	private Type testType;
	private UploadFileItem validUploadFileItem;

	@Mock
	private PictureUtils pictureUtils;

	@Mock
	TypeManager typeManager;

	@Mock
	FileInfoManager fileInfoManager;

	@Mock
	SaveFileUtils saveFileUtils;

	@Before
	public void setUp(){
		uploadControllerPartialMocked = new UploadController(){
			@Override
			protected void unzipFile(String filePath) throws IOException {}

			@Override
			protected List<String> getPathesOfPictures(String folderPath) {
				return testListOfPicturePathes;
			}

			@Override
			protected String findImageWithConcreteSize(String pathPrefix, List<String> filePathes, int height, int width, int accuracy) {
				return PICTURE_PATH;
			}

			@Override
			protected boolean validateZip(CommonsMultipartFile file) {
				return true;
			}

			@Override
			protected void deleteDirectory(String directory) throws IOException {}

			@Override
			protected void createFilesAndStoreMultipartFile(File pathToFileDirFile,
															File transferZipFile,
															UploadFileItem uploadItem) throws IOException {}
		};

		testType = new Type();
		testListOfPicturePathes = new ArrayList<String>();

		when(fileInfoManager.isNameOccupied(OCCUPIED_FILE_INFO_NAME)).thenReturn(true);
		when(fileInfoManager.isHashContains(HASH)).thenReturn(true);
		when(typeManager.getTypeById(TYPE_ID)).thenReturn(testType);
		when(typeManager.isTypeByIdContains(TYPE_ID_STRING)).thenReturn(true);

		uploadControllerPartialMocked.setFileInfoManager(fileInfoManager);
		uploadControllerPartialMocked.setTypeManager(typeManager);

		validUploadFileItem = new UploadFileItem(){
			@Override
			public String getFileName() {
				return FILENAME;
			}
		};
		validUploadFileItem.setName(NAME);
		validUploadFileItem.setDescription(DESC);
		validUploadFileItem.setType(TYPE_ID_STRING);
	}

	@Test
	public void testUnzipFileAddPictures(){
		uploadControllerPartialMocked.unzipFileAddPictures(PICTURE_PATH, FOLDER_PATH, new FileInfo());

		FileInfo fileInfo = new FileInfo();
		fileInfo.setIcon(PICTURE_PATH);
		fileInfo.setMainTitle(PICTURE_PATH);

		ArgumentCaptor<FileInfo> argument = ArgumentCaptor.forClass(FileInfo.class);

		verify(fileInfoManager).saveFileInfo(argument.capture());
		assertEquals(PICTURE_PATH, argument.getValue().getIcon());
		assertEquals(PICTURE_PATH, argument.getValue().getMainTitle());
	}

	@Test
	public void shouldCreateFileInfoCorrectly(){
		FileInfo fi = uploadControllerPartialMocked.createFileInfo(validUploadFileItem,HASH);

		assertEquals("name inputted wrong in FI",NAME, fi.getName());
		assertEquals("description inputted wrong in FI",DESC, fi.getDescription());
		assertEquals("filePath  inputted wrong in FI",FILENAME, fi.getFilePath());
		assertEquals("hash inputted wrong in FI",HASH, fi.getHash());
		assertEquals("type inputted wrong in FI",testType, fi.getType());
		assertNull("icon inputted in FI?? 0_o", fi.getIcon());
		assertNull("id inputted in FI?? 0_o", fi.getIcon());
		assertNull("mainTitle inputted in FI?? 0_o", fi.getMainTitle());
		assertNotNull("createDate is null; (check FileInfo constructor)", fi.getCreatedDate());
	}

	@Test
	public void testValidateValideUploadItem(){
		Model model = new ExtendedModelMap();
		assertTrue(uploadControllerPartialMocked.validateUploadItem(validUploadFileItem, model));

		assertNull("created NAME_NOT_VALID=> validation fail",model.asMap().get(UploadController.NAME_NOT_VALID));
		assertNull("created DESC_NOT_VALID=> validation fail",model.asMap().get(UploadController.DESC_NOT_VALID));
		assertNull("created FILE_NOT_VALID=> validation fail",model.asMap().get(UploadController.FILE_NOT_VALID));
		assertNull("created NAME_OCCUPIED=> validation fail",model.asMap().get(UploadController.NAME_OCCUPIED));
		assertNull("created TYPE_INVALID=> validation fail",model.asMap().get(UploadController.TYPE_INVALID));
	}

	@Test
	public void testValidateUploadNotValidName_Description_Type_Filename(){
		UploadFileItem nonValid = new UploadFileItem(){
			@Override
			public String getFileName() {
				return "";
			}
		};
		nonValid.setName("");
		nonValid.setDescription(new String(new char[226]));
		nonValid.setType("");
		Model model = new ExtendedModelMap();
		assertFalse(uploadControllerPartialMocked.validateUploadItem(nonValid, model));

		assertTrue("not created NAME_NOT_VALID validation passed when should fails",
				(Boolean) model.asMap().get(UploadController.NAME_NOT_VALID));
		assertTrue("not created DESC_NOT_VALID validation passed when should fails",
				(Boolean) model.asMap().get(UploadController.DESC_NOT_VALID));
		assertTrue("not created FILE_NOT_VALID validation passed when should fails",
				(Boolean)model.asMap().get(UploadController.FILE_NOT_VALID));
		assertTrue("not created TYPE_INVALID validation passed when should fails",
				(Boolean) model.asMap().get(UploadController.TYPE_INVALID));
		assertNull("created NAME_OCCUPIED=> validation fail",model.asMap().get(UploadController.NAME_OCCUPIED));
	}

	@Test
	public void testValidateUploadNotValidNameOccupied(){
		UploadFileItem nonValid = new UploadFileItem(){
			@Override
			public String getFileName() {
				return ".zip";
			}
		};
		nonValid.setName(OCCUPIED_FILE_INFO_NAME);
		nonValid.setDescription("");
		nonValid.setType(TYPE_ID_STRING);
		nonValid.setName(OCCUPIED_FILE_INFO_NAME);
		Model model = new ExtendedModelMap();
		assertFalse(uploadControllerPartialMocked.validateUploadItem(nonValid, model));

		assertTrue("not created NAME_OCCUPIED validation passed when should fails",
				(Boolean) model.asMap().get(UploadController.NAME_OCCUPIED));
		assertNull("created NAME_NOT_VALID=> validation fail", model.asMap().get(UploadController.NAME_NOT_VALID));
		assertNull("created DESC_NOT_VALID=> validation fail", model.asMap().get(UploadController.DESC_NOT_VALID));
		assertNull("created FILE_NOT_VALID=> validation fail", model.asMap().get(UploadController.FILE_NOT_VALID));
		assertNull("created TYPE_INVALID=> validation fail",model.asMap().get(UploadController.TYPE_INVALID));
	}

	@Test
	public void shouldPassValidateZipHash(){
		assertTrue(uploadControllerPartialMocked.validateZipHash("hereIsSomeStringThat", FOLDER_PATH));
	}

	@Test
	public void shouldFailValidateZipHash(){
		assertFalse(uploadControllerPartialMocked.validateZipHash(HASH, FOLDER_PATH));
	}

}
