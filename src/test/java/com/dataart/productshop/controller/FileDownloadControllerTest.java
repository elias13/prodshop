package com.dataart.productshop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;

@RunWith(MockitoJUnitRunner.class)
public class FileDownloadControllerTest extends ControllerModule{

    private FileDownloadController controller;

    @Mock
    private FileInfoManager fiManager;


    private static final String prePath = "src/test/resources/testData";

    @Before
    public void setUp(){
		assertNotNull(fiManager);
        when(fiManager.getByName(Mockito.anyString())).thenReturn(new FileInfo());


        controller = new FileDownloadController();
		controller.setFileInfoManager(fiManager);
		controller.setFilePrePath(prePath);
    }

    @Test
    public void shouldAllowToDownloadFile() throws IOException {
        String filename = "testFilename";
        String name = "testFolderName";
        File testFolder = null;
        File filenameFile = null;
        try{

            testFolder = new File(prePath + "/" + name);
            testFolder.mkdirs();
            filenameFile = new File(testFolder.getPath() + "/" + filename + ".zip");
            filenameFile.createNewFile();

            String response = controller.checkDownload(filename, name);
            assertEquals("true", response);
        } finally {
            filenameFile.delete();
            testFolder.delete();
        }

        assertFalse(filenameFile.exists());
        assertFalse(testFolder.exists());
    }

    @Test
    public void checkDownloadShouldFail(){
        String filename = "someFilenameThatNotContains";
        String name = "someFolderNameThatNotContains";
        String response = controller.checkDownload(filename, name);
        assertEquals("", response);
    }

    @Test
    public void shouldNotStartsDownload() throws IOException {
        String filename = "someFilenameThatNotContains";
        String name = "someFolderNameThatNotContains";
        HttpServletResponse mockedResponse = Mockito.mock(HttpServletResponse.class);
        controller.downloadFile(filename, name, mockedResponse);

		Mockito.verifyNoMoreInteractions(mockedResponse);
    }

	@Test
	public void controllerShouldStoreReturnFileInResponse() throws IOException {
		String filename = "testFilename";
		String name = "testFolderName";
		File testFolder = null;
		File filenameFile = null;
		try{

			testFolder = new File(prePath + "/" + name);
			testFolder.mkdirs();
			filenameFile = new File(testFolder.getPath() + "/" + filename + ".zip");
			HttpServletResponse mockedResponse = Mockito.mock(HttpServletResponse.class);
			controller.downloadFile(filename, name, mockedResponse);
			filenameFile.createNewFile();

			controller.downloadFile(filename, name, mockedResponse);

			verify(mockedResponse).getWriter();
			verify(mockedResponse).setContentType("application/txt");
			verify(mockedResponse).flushBuffer();
		} finally {
			try{
				filenameFile.delete();
			}finally {
				testFolder.delete();
			}
		}
		assertFalse(filenameFile.exists());
		assertFalse(testFolder.exists());
	}
}
