package com.dataart.productshop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;

@RunWith(MockitoJUnitRunner.class)
public class FilePageControllerTest extends ControllerModule {

	private static final String validName = "someValidName";
	private static final String invalidName = "someInvalidName";

	@Mock
	private FileInfoManager fiManager;

	private FilePageController controller;
	private FileInfo fileInfo;

	@Before
	public void setUp(){
		assertNotNull(fiManager);

		fileInfo = new FileInfo();
		fileInfo.setName(validName);
		when(fiManager.getByName(validName)).thenReturn(fileInfo);

		controller = new FilePageController();
		controller.setFileInfoManager(fiManager);
	}

	@Test
	public void methodGetFileHelloPageShouldReturnFileHelloPage(){
		ModelAndView actualMav = controller.getFileHelloPage(validName);

		assertNotNull(actualMav);
		assertEquals(FilePageController.FILE_PAGE_NAME, actualMav.getViewName());
		assertEquals(fileInfo, actualMav.getModel().get(FilePageController.ITEM_MODEL_NAME));
	}

	@Test
	public void methodGetFilePageAcynchShouldReturnFileHelloPage(){
		ModelAndView actualMav = controller.getFilePageAcynch(validName);

		assertNotNull("actual Map is null",actualMav);
		assertEquals(" view name does not calls correctly",FilePageController.ASYNCH_FILE_PAGE_NAME, actualMav.getViewName());
		assertEquals(" not get correct fileInfo(mocked FileInfo) in modelAndView.model"
				,fileInfo, actualMav.getModel().get(FilePageController.ITEM_MODEL_NAME));
	}

	@Test
	public void methodGetFileHelloPageShouldReturnFileHelloPageWithoutFileInfoInModelInMAVObject(){
		ModelAndView actualMav = controller.getFileHelloPage(invalidName);

		assertNotNull("actual Map is null", actualMav);
		assertEquals(" view name does not calls correctly",FilePageController.FILE_PAGE_NAME, actualMav.getViewName());
		assertNull(" not get null fileInfo(mocked FileInfo) in modelAndView.model; maybe mock works wrong"
				, actualMav.getModel().get(FilePageController.ITEM_MODEL_NAME));
	}

	@Test
	public void methodGetFilePageAcynchShouldReturnFileHelloPageWithoutFileInfoInModelInMAVObject(){
		ModelAndView actualMav = controller.getFilePageAcynch(invalidName);

		assertNotNull("actual Map is null", actualMav);
		assertEquals(" view name does not calls correctly",FilePageController.ASYNCH_FILE_PAGE_NAME, actualMav.getViewName());
		assertNull(" not get null fileInfo(mocked FileInfo) in modelAndView.model; maybe mock works wrong"
				, actualMav.getModel().get(FilePageController.ITEM_MODEL_NAME));
	}


}
