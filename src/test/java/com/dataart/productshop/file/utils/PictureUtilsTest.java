package com.dataart.productshop.file.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import com.dataart.productshop.Constants;
import com.dataart.productshop.testutil.SpringConfigTest;
import junit.framework.Assert;

public class PictureUtilsTest extends SpringConfigTest {

    private static final String FILENAME1 = "pictureNumber1.jpg";
    private static final String FILENAME2 = "pictureNumber2.jpg";

    @Value(Constants.PROP_FILE_PATH)
    String filePath;

    @Test
    public void shouldCreateThePicturesThenFindThemAutomatically() throws IOException {

        File file1 = new File(filePath + "/" + FILENAME1);
        File file2 = new File(filePath + "/" + FILENAME2);

        file1.createNewFile();
        file2.createNewFile();

        List<String> expectedFileNames = Arrays.asList(FILENAME1, FILENAME2);

        List<String> foundFilePathes = PictureUtils.getPathesOfPictures(filePath);

        Assert.assertTrue("found collection :\n"
				+ foundFilePathes
				+ " doesn't contains expected collection :\n"
				+  expectedFileNames,foundFilePathes.containsAll(expectedFileNames));

        file1.delete();
        file2.delete();

    }
}
