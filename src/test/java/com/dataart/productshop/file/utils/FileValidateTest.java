package com.dataart.productshop.file.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.Test;


public class FileValidateTest{

	public static final String FOLEDER_PATH = "src/test/resources/testData/";
	/**
	 * see documentation for detailed explanation valide zip files
	 * also see src/test/resources/testData/testValidZipFile.zip
	 * and src/test/resources/testData/testNotValidZipFile.zip
	 */
	public static final String VALID_FILE_PATH = "testValidZipFile.zip";
	public static final String NON_VALID_FILE_PATH = "testNotValidZipFile.zip";

	@Test
	public void checkZipFilenameValidate() throws IOException {
		String validFilename_1 = "someNormal.zip";
		String validFilename_2 = "some__1&normal.zip";
		String notValidFilename_1 = "someUnnormal.zips";
		String notValidFilename_2 = "someUnnormal.";
		String notValidFilename_3 = "someUnnormal.ddszip";
		assertTrue(FileValidate.validateZipFilename(validFilename_1));
		assertTrue(FileValidate.validateZipFilename(validFilename_2));
		assertFalse(FileValidate.validateZipFilename(notValidFilename_1));
		assertFalse(FileValidate.validateZipFilename(notValidFilename_2));
		assertFalse(FileValidate.validateZipFilename(notValidFilename_3));
	}
	
	@Test
	public void checkZipFile() throws Exception{
		File valideZipFile = new File(FOLEDER_PATH + VALID_FILE_PATH);
		File nonValideZipFile = new File(FOLEDER_PATH + NON_VALID_FILE_PATH);

		assertTrue(valideZipFile.exists());
		assertTrue(nonValideZipFile.exists());

		FileInputStream fisValide = new FileInputStream(valideZipFile);
		assertTrue(FileValidate.validateConvertZip(fisValide));
		
		FileInputStream fisNonValide = new FileInputStream(nonValideZipFile);
		assertFalse(FileValidate.validateConvertZip(fisNonValide));
		
	}
}
