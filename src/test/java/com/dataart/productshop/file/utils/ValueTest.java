package com.dataart.productshop.file.utils;

import com.dataart.productshop.dao.impl.BaseDaoTestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ValueTest extends BaseDaoTestCase{

	@Value("${store.path}")
	private String storeFilePath;
	
	public static final String EXPECT_STRING = "src/test/webapp/images/snow";
	
	@Test
	public void valuePropertyTest(){
		assertNotNull(storeFilePath);
		assertEquals(EXPECT_STRING, storeFilePath);
		
	}
}
