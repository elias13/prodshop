package com.dataart.productshop.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.dataart.productshop.dao.FileInfoDao;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.impl.FileInfoManagerImpl;

public class FileInfoManagerTest {

	private static final int SIZE = 5;


	@Test
	public void getFullFiveShouldReturnFiveElementsInEmptyListCase(){
		FileInfoManagerImpl manager = new FileInfoManagerImpl();
		FileInfoDao mockedDao = mock(FileInfoDao.class);
		List<FileInfo> fileInfoList1 = new ArrayList<FileInfo>();

		when(mockedDao.getByDownloads(anyInt(), anyInt())).thenReturn(fileInfoList1);

		manager.setFileInfoDao(mockedDao);

		List<FileInfo> actualFileInfos = manager.getFullFive();
		assertNotNull("got null list on result", actualFileInfos);
		assertEquals("wrong result on empty list", SIZE, actualFileInfos.size());
	}

	@Test
	public void getFullFiveShouldReturnFiveElementsInPartListCase(){
		FileInfoManagerImpl manager = new FileInfoManagerImpl();
		FileInfoDao mockedDao = mock(FileInfoDao.class);
		List<FileInfo> fileInfoList1 = new ArrayList<FileInfo>();
		fileInfoList1.add(new FileInfo());
		fileInfoList1.add(new FileInfo());

		when(mockedDao.getByDownloads(anyInt(), anyInt())).thenReturn(fileInfoList1);

		manager.setFileInfoDao(mockedDao);

		List<FileInfo> actualFileInfos = manager.getFullFive();
		assertNotNull("got null list on result", actualFileInfos);
		assertEquals("wrong result on empty list", SIZE, actualFileInfos.size());

		int counter = 0;
		for (FileInfo actualFileInfo : actualFileInfos) {
			if(actualFileInfo != null){
				counter ++;
			}
		}

		assertEquals("the number of FileInfo not fits real", 2, counter);
	}

	@Test
	public void getFullFiveShouldReturnFiveElementsInNullListCase(){
		FileInfoManagerImpl manager = new FileInfoManagerImpl();
		FileInfoDao mockedDao = mock(FileInfoDao.class);
		when(mockedDao.getByDownloads(anyInt(), anyInt())).thenReturn(null);

		manager.setFileInfoDao(mockedDao);

		List<FileInfo> actualFileInfos = manager.getFullFive();
		assertNotNull("got null list on result", actualFileInfos);
		assertEquals("wrong result on empty list", SIZE, actualFileInfos.size());
	}

}
