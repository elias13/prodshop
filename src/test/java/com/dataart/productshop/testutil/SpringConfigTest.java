package com.dataart.productshop.testutil;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dataart.productshop.dao.FileInfoDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/spring/application-context.xml")
public class SpringConfigTest {
	
	@Autowired
   FileInfoDao dao;
	
	@Test
	public void testAutowiring(){
		assertNotNull(dao);
	}

}
