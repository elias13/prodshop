
CREATE SCHEMA `teststore` ;


CREATE  TABLE `teststore`.`type` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `name` VARCHAR(225) NOT NULL ,

  PRIMARY KEY (`id`) );
  

CREATE  TABLE `teststore`.`fileinfo` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `name` VARCHAR(225) NOT NULL ,
  
  `description` VARCHAR(225),
  
  `file_path` VARCHAR(225) NOT NULL ,

  `downloads` BIGINT NOT NULL DEFAULT 0 ,

  `created_date` DATETIME NOT NULL ,
  
  `type_id` BIGINT NOT NULL,
  
  PRIMARY KEY (`id`),
  
  FOREIGN KEY (type_id) references type(id));
  
  
CREATE  TABLE `teststore`.`picture` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `file_path` VARCHAR(225) NOT NULL ,
  
  `file_info_id` BIGINT NOT NULL,

  PRIMARY KEY (`id`),
  
  FOREIGN KEY (file_info_id) references fileinfo(id));

