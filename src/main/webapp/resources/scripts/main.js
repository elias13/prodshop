
var prePath;

/*upload the file after click the button download*/
function upload(element, prePath, name, path){

    var currentDownloadCount = $(element).closest(".span5").find("#times");
    $.ajax({
        type:"GET",
        url: prePath + 'check/' + name + '/' + path,
        success:function(htmlRequest){
            if(htmlRequest){
                window.location.href = prePath + 'download/' + name + '/' + path;
                currentDownloadCount.text(1 + parseInt(currentDownloadCount.text()));
                $("#popular_grid").parent().fadeOut("fast");
                setTimeout(function(prePath){reloadHeader(prePath)}, 1000);
            }
        }
    });
}

/* reload popular images*/
function reloadHeader(){

    var heresome = "";
    $.ajax({
        type:"GET",
        url: prePath + "popularGrid",
        success:function(resultHtml){
            /*$("#popular_grid").parent().html(resultHtml);*/
            var parent = $("#popular_grid").parent();
            parent.html(resultHtml);
            parent.fadeIn("fast");
        }
    });
}

 /* when select category in main menu - the appropriate data will download*/
function executeCategoryListAction(prePath){
    $("#categoryList > li").not(".nav-header").click(function(){
        $("#categoryList .active").removeClass("active");
        $(this).addClass("active");

        var id = $(this).attr("id");
        $.ajax({type: "POST",
            url: prePath + '/loadCategory',
            data: "id=" + id,
            success: function (responseHtml) {
                $("#fileInfoContent").html(responseHtml.trim());
                executeFileContainerAction();
            },
            error: function (){

            }
        });

    }) ;
}


/* show the detailed file data on click the row in main-view*/
function executeFileContainerAction(prePath){
    $("#fileInfoContainer div.row").click(function() {
        var container = $(this).next();
        if(container.is(':visible')){
            container.slideUp('slow')
        } else {
            $.ajax({
                type:"POST",
                url: $(this).find("a").attr("href"),
                success: function(resultHtml){
                    container.html(resultHtml);
                    container.slideDown("slow")
                }
            });
        }
    });

}