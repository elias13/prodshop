<%@ include file="taglib.jsp" %>
<%@ page language="java" errorPage="/error.jsp" contentType="text/html; charset=utf-8" %>
<meta charset='utf-8'>
<!DOCTYPE html>

<html lang="en" manifest="/cache.manifest">
<head>

</head>
<body>


<div class="container">

    <jsp:include page="inner/header.jsp" />

    <div id="main_content" class="container">
        <jsp:include page="inner/mainContent.jsp" />
    </div>

    <footer class="modal-footer navbar-fixed-bottom">
        by Illia Pogodin
    </footer>
    <hidden id="currentForm" value = "${currentForm}"/>

</div>


</body>
<script type="text/javascript">

    var prePath = <c:url value="/"/>;
    $(document).ready(function(){
        executeCategoryListAction(prePath);
        prePath = '<c:url value="/"/>';
    });
</script>

</html>

