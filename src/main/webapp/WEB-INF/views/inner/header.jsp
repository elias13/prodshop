<%@ page language="java" contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <ul id="topFooter" class="nav">
                    <li     <c:if test="${active_header == 'home'}">
                                class = "active"
                            </c:if>
                    >
                        <a id="home" href="<c:url value='/'/>">Home</a>
                    </li>
                    <li     <c:if test="${active_header == 'upload'}">
                        class = "active"
                    </c:if>
                            >
                        <a id="upload" href="<c:url value='/upload'/>">Upload</a>
                        <%--href='<c:url value = "/upload"/>'--%>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<br/>
<br/>
</html>
<script type="text/javascript">

</script>


