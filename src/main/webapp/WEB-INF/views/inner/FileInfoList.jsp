<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" errorPage="/error.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>

<div id="fileInfoContainer" class="container">
    <c:choose>
        <c:when test="${not empty fileInfos}">
            <c:forEach var="item" items="${fileInfos}">
                <div class="row">
                    <div class="span6">
                        <div class="span1">
                            <a href='<c:url value="/item/${item.name}"/>' class="brand"/>
                            <c:choose>
                                <c:when test="${not empty item.icon}">
                                    <img class="img-rounded" src='<c:url value = "/store/${item.name}/${item.icon}"/>'
                                         alt="${item.name}" title="${item.name}"/>
                                </c:when>
                                <c:otherwise>
                                    <img class="img-rounded" src='<c:url value = "/image/base/no.jpg"/>' alt="${item.name}" title="${item.name}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        ${item.name}
                    </div>
                </div>
                <div class="container" style="display: none">
                </div>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <li class="nav-header text-left">
                no files for this category yet
            </li>
        </c:otherwise>
    </c:choose>
</div>
</html>
<script type="text/javascript">


   /* $("#fileInfoContainer div.row").click(function() {


        var container = $(this).next();
        if(container.is(':visible')){
            container.slideUp('slow')
        } else {
            $.ajax({
                type:"POST",
                url: $(this).find("a").attr("href"),
                success: function(resultHtml){
                    container.html(resultHtml);
                    container.slideDown("slow")
                }
            });
        }
    });*/

</script>