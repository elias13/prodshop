<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<div class="container span8 rounded">
    <div class="row">
        <div id="titleIcon" class="span2">
            <a href = '<c:url value="/item/${selectedItem.name}"/> '>
                <c:choose>
                    <c:when test="${not empty selectedItem.mainTitle}">
                        <img class="img-rounded" src='<c:url value = "/store/${selectedItem.name}/${selectedItem.mainTitle}"/>'
                             alt="${selectedItem.name}" title="${selectedItem.name}"/>
                    </c:when>
                    <c:otherwise>
                        <img class="img-rounded" src='<c:url value = "/image/base/no.jpg"/>'
                             alt="${selectedItem.name}" title="${selectedItem.name}"/>
                    </c:otherwise>
                </c:choose>
            </a>
        </div>
        <div class="span5">

            <div class="row nav-header">
                name : ${selectedItem.name}
            </div>
            <span class="span4 cut-string">
                description : ${selectedItem.description}
            </span>
            <span class="span4">
                downloaded <span id = "times">${selectedItem.downloads}</span> times;
                <br>
                stored on :  <fmt:formatDate type="both" value="${selectedItem.createdDate}"/>
            </span>
            <br><br><br>
            <button class="btn btn-primary btn-block"
                    onclick="upload(this, <c:url value="/"/>, '${selectedItem.name}', '${selectedItem.filePath}')">download the inner file</button>
        </div>
    </div>
</div>
</html>

