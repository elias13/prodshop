<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>

<body>

<div class="container">
    <div class="row">
        <jsp:include page="popularGrid.jsp"/>
    </div>

    <br>
    <br>

    <div class="row main_content">

        <div class="span2">
            <div class="well">
                <nav>
                    <c:if test="${not empty categories}">
                        <ul id="categoryList" class="nav nav-list">
                            <li class="nav-header" disable="disable">
                                categories
                            </li>
                            <c:forEach var="category" items="${categories}" varStatus="id">
                                <li id="${category.id}">
                                    <a href="#">
                                        ${category.name}
                                    </a>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:if>
                </nav>
            </div>
        </div>

        <div class="span10">
            <div id="fileInfoContent" class="well text-center">
                <img class="img-rounded" src='<c:url value="/image/base/main_title.gif"/>'>
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">

    function hello(){

    }


</script>
</html>