package com.dataart.productshop.dao;

import java.util.List;

public interface BaseDao<T> {
	T getById (Long id);
	
	void save (T entity);
	
	List<T> getAll();
	
	void delete (Long id);
}
