package com.dataart.productshop.dao;

import com.dataart.productshop.model.Type;

public interface TypeDao extends BaseDao<Type>{

    Type getByName(String name);
	
}
