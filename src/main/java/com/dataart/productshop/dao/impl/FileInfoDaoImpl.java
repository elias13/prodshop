package com.dataart.productshop.dao.impl;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.dataart.productshop.dao.FileInfoDao;
import com.dataart.productshop.model.FileInfo;

@Repository
public class FileInfoDaoImpl extends BaseDaoImpl<FileInfo> implements
		FileInfoDao {

	public FileInfoDaoImpl() {
		super(FileInfo.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FileInfo> getByDownloads(int from, int amount) {
		return createCriteria(FileInfo.class).addOrder(Order.desc("downloads"))
				.addOrder(Order.desc("createdDate")).setMaxResults(amount)
				.setFirstResult(from).list();
	}

	@Override
	public List<FileInfo> getFileInfoByTypeId(Long typeId) {
		return createCriteria(FileInfo.class).add(Restrictions.eq("type.id", typeId))
				.addOrder(Order.desc("downloads"))
				.addOrder(Order.desc("createdDate")).list();

	}

	@Override
	public FileInfo getByName(String name) {
        return getResultFromList(createCriteria(FileInfo.class)
                .add(Restrictions.eq("name", name)).list());
	}

    @Override
    public FileInfo getByHash(String hash) {
        return getResultFromList(createCriteria(FileInfo.class)
                .add(Restrictions.eq("hash", hash)).list());
    }
}
