package com.dataart.productshop.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.dataart.productshop.dao.TypeDao;
import com.dataart.productshop.model.Type;

@Repository
public class TypeDaoImpl extends BaseDaoImpl<Type>implements TypeDao{

	public TypeDaoImpl() {
		super(Type.class);
	}

    @Override
    public Type getByName(String name) {
        return getResultFromList(createCriteria(Type.class)
                .add(Restrictions.eq("name", name)).list());
    }

}
