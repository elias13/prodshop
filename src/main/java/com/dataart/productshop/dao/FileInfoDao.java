package com.dataart.productshop.dao;

import java.util.List;

import com.dataart.productshop.model.FileInfo;

public interface FileInfoDao extends BaseDao<FileInfo> {
	/**
	 * get List of FileInfos places in decade order by downloads and date
	 *
	 * @param from   number of element, in sorted by download and date list, which started
	 *               to put in list from
	 * @param amount number of elements which will be max put in list
	 * @return list of FileInfo elements
	 */
	List<FileInfo> getByDownloads(int from, int amount);

	List<FileInfo> getFileInfoByTypeId(Long typeId);

	/**
	 * in DB we should store FileInfo unique by name
	 * so returning type doesn't list
	 * @param name the name of FileInfo
	 * @return file info with appropriate name
	 */
	FileInfo getByName(String name);

    FileInfo getByHash(String hash);


}
