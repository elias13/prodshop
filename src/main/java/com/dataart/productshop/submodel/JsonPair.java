package com.dataart.productshop.submodel;

public class JsonPair {
    private boolean valid;

    public JsonPair(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
