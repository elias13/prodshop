package com.dataart.productshop.submodel;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UploadFileItem {
	private String name;
	private String description;
	private String type;
	private CommonsMultipartFile fileData;
    private boolean validateFile;
	
	public UploadFileItem() {
		validateFile = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CommonsMultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}

	public String getFileName(){
		if(fileData != null){
			return fileData.getOriginalFilename();
		}
		return null;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    public boolean isValidateFile() {
        return validateFile;
    }

    public void setValidateFile(boolean validateFile) {
        this.validateFile = validateFile;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof UploadFileItem)) return false;

		UploadFileItem that = (UploadFileItem) o;

		if (validateFile != that.validateFile) return false;
		if (description != null ? !description.equals(that.description) : that.description != null) return false;
		if (fileData != null ? !fileData.equals(that.fileData) : that.fileData != null) return false;
		if (name != null ? !name.equals(that.name) : that.name != null) return false;
		if (type != null ? !type.equals(that.type) : that.type != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (fileData != null ? fileData.hashCode() : 0);
		result = 31 * result + (validateFile ? 1 : 0);
		return result;
	}

	public UploadFileItem clone(){
		UploadFileItem item = new UploadFileItem();
		item.setDescription(description);
		item.setName(name);
		item.setFileData(fileData);
		item.setValidateFile(validateFile);
		item.setType(type);
		return item;
	}
}