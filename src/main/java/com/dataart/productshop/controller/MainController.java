package com.dataart.productshop.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dataart.productshop.Constants;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;
import com.dataart.productshop.service.TypeManager;

/**
 * Handles requests for the application's home page.
 */
@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	/*------            view names            ------*/
	/* homePage*/
	final static String HOME_PAGE = "home";
	/* all center content on homePage*/
	final static String MAIN_CONTENT_VIEW = "inner/mainContent";
	/* main content with list of FileInfo on center of homePage*/
	final static String FILE_INFO_LIST_VIEW = "inner/fileInfoList";
	/* panel with popular files on top*/
	final static String POPULAR_GRID_VIEW = "inner/popularGrid";

	/*------            model names            ------*/
	/* name of file-info-collection on top(inner/popularGrid - view = top of homePage)*/
	final static String FILE_INFO_POPULAR_FILES = "popular";
	/* actually TYPES ; name of all-types-collection (the panel places left on homePage)*/
	final static String CATEGORIES = "categories";
	/* name of file-info-collection that relates to selected type on homePage*/
	final static String FILE_INFOS = "fileInfos";

    @Autowired
    FileInfoManager fileInfoManager;

	@Autowired
	TypeManager typeManager;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView initHomePage() {
		return getMavDataForHomePage(HOME_PAGE);
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView getHomeAsynch(){
		return getMavDataForHomePage(MAIN_CONTENT_VIEW);
	}

	@RequestMapping(value = "/loadCategory", method = RequestMethod.POST)
	public ModelAndView loadFileInfoByCategory(@RequestParam("id") long id){
		List<FileInfo> fileInfos = fileInfoManager.getFileInfoByTypeId(id);
		return new ModelAndView(FILE_INFO_LIST_VIEW, FILE_INFOS, fileInfos);
	}

	@RequestMapping(value = "/popularGrid", method = RequestMethod.GET)
	public ModelAndView getPopularGrid(){
		return new ModelAndView(POPULAR_GRID_VIEW, FILE_INFO_POPULAR_FILES, fileInfoManager.getFullFive());
	}

	private ModelAndView getMavDataForHomePage(String viewName){
		ModelAndView mav = new ModelAndView();

		mav.addObject(FILE_INFO_POPULAR_FILES, fileInfoManager.getFullFive());
		mav.addObject(CATEGORIES, typeManager.getAll());
		/**
		 * represents - what id current active-header in the top of page (home and upload page)
		 */
		mav.addObject(Constants.ACTIVE_HEADER, HOME_PAGE);
		mav.setViewName(viewName);
		return mav;
	}

	public void setFileInfoManager(FileInfoManager fileInfoManager) {
		this.fileInfoManager = fileInfoManager;
	}

	public void setTypeManager(TypeManager typeManager) {
		this.typeManager = typeManager;
	}
}
