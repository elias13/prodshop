package com.dataart.productshop.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dataart.productshop.Constants;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;

@Controller
public class FileDownloadController {

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	FileInfoManager fileInfoManager;

	@Value(Constants.PROP_FILE_PATH)
	private String filePrePath;

	@RequestMapping(value = "/download/{name}/{filename}", method = RequestMethod.GET)
	public void downloadFile(@PathVariable("filename") String filename,
							   @PathVariable("name") String name,
							   HttpServletResponse response){
        InputStream is = null;
		try{
			System.out.println(filename);
		    is = new FileInputStream(new File(filePrePath + "/" + name + "/" + filename + ".zip"));
			IOUtils.copy(is, response.getWriter());
			response.setContentType("application/txt");
			response.flushBuffer();
		}catch(IOException e){
			logger.error("can't find file in downloadFile method "
					+ e.getMessage() + " " + e.getStackTrace());
		}finally {
            IOUtils.closeQuietly(is);
        }

		FileInfo fileInfo = fileInfoManager.getByName(name);
		fileInfo.setDownloads(fileInfo.getDownloads() + 1);
		fileInfoManager.saveFileInfo(fileInfo);
	}

	@RequestMapping(value = "/check/{name}/{filename}.zip", method = RequestMethod.GET)
	public @ResponseBody String checkDownload(@PathVariable("filename") String filename,
							 @PathVariable("name") String name){
		return new File(filePrePath + "/" + name + "/" + filename + ".zip").exists() ? "true" : new String();
	}

	public void setFileInfoManager(FileInfoManager fileInfoManager) {
		this.fileInfoManager = fileInfoManager;
	}

	public void setFilePrePath(String filePrePath) {
		this.filePrePath = filePrePath;
	}
}
