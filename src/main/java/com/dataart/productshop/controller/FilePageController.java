package com.dataart.productshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dataart.productshop.service.FileInfoManager;

@Controller
public class FilePageController {

	static final String FILE_PAGE_NAME = "filePage";
	static final String ASYNCH_FILE_PAGE_NAME = "inner/filePage";
	static final String ITEM_MODEL_NAME = "selectedItem";

	@Autowired
	FileInfoManager fileInfoManager;
	
	@RequestMapping(value = "/item/{itemName}", method = RequestMethod.GET)
	public ModelAndView getFileHelloPage(@PathVariable("itemName")String itemName){
		return new ModelAndView(FILE_PAGE_NAME,ITEM_MODEL_NAME
                ,fileInfoManager.getByName(itemName));
	}

    @RequestMapping(value = "/item/{itemName}", method = RequestMethod.POST)
    public ModelAndView getFilePageAcynch(@PathVariable("itemName")String itemName){
        return new ModelAndView(ASYNCH_FILE_PAGE_NAME,ITEM_MODEL_NAME
                ,fileInfoManager.getByName(itemName));
    }

	public void setFileInfoManager(FileInfoManager fileInfoManager) {
		this.fileInfoManager = fileInfoManager;
	}
}
