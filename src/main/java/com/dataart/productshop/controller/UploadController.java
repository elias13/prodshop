package com.dataart.productshop.controller;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.dataart.productshop.Constants;
import com.dataart.productshop.submodel.UploadFileItem;
import com.dataart.productshop.file.utils.FileValidate;
import com.dataart.productshop.file.utils.PictureUtils;
import com.dataart.productshop.file.utils.SaveFileUtils;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;
import com.dataart.productshop.service.TypeManager;
import com.dataart.productshop.submodel.JsonPair;

@Controller
@RequestMapping("/upload")
public class UploadController {

	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);

	/*------            view names            ------*/
	public static final String START_UPLOAD_PAGE = "upload";

	/*------            model names            ------*/
	public static final String TYPES = "types";
	public static final String ITEM_NAME = "uploadItem";
	public static final String UPLOAD = "upload";;

    public static final String NAME_NOT_VALID = "nameValidationFailed";
    public static final String DESC_NOT_VALID = "descrValidationFailed";
    public static final String FILE_NOT_VALID = "fileValidationFailed";
    public static final String NAME_OCCUPIED = "nameOccupied";
    public static final String TYPE_INVALID = "typeNotSelected";
    public static final String ZIP_OCCUPIED = "notuniqueZip";

	/*------            other constant names            ------*/
    public static final int TITLE_IMAGE_SIZE = 512;
	public static final int ICON_IMAGE_SIZE = 128;
    public static final String ZIP_EXTENTION = ".zip";

	/**
	 * this field should be <= max size of column FileInfo.description in database
	 * if you use "creating tables.sql" for creating database => change numbers in 	description field
	 */
	public static final int DESCR_SIZE = 225;

	/**
	 * accuracy for image size;
	 * feel free to change this
	 * for now icon images could be all images in zip files with size from 126x126 to 128x128
	 */
	public static final int ACCURACY = 2;

	/**
	 * pre path for storing files
	 * see : Constants.PROP_FILE_PATH and resource/data/file.store.properties
	 */
	@Value(Constants.PROP_FILE_PATH)
	private String filePath;
	
	@Autowired
	private FileInfoManager fileInfoManager;
	
	@Autowired
	private TypeManager typeManager;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getUploadForm(Model model) {
		model.addAttribute(ITEM_NAME, new UploadFileItem());
		model.addAttribute(TYPES, typeManager.getAll());
		model.addAttribute(Constants.ACTIVE_HEADER, UPLOAD);
		return START_UPLOAD_PAGE;
	}

    //ToDO stored for acynch validate
    @RequestMapping(value="/name",method=RequestMethod.POST)
    public @ResponseBody JsonPair checkName(@RequestParam(value="name") String name){
        return new JsonPair(name != null && !name.isEmpty()
                && !fileInfoManager.isNameOccupied(name));
    }

    //ToDO stored for acynch validate
    @RequestMapping(value="/description",method=RequestMethod.POST)
    public @ResponseBody JsonPair checkDesc(@RequestParam(value="description") String description){
        return new JsonPair(description != null && !description.isEmpty()
                && !fileInfoManager.isNameOccupied(description));
    }


	@RequestMapping(method = RequestMethod.POST)
	public String storeFile(@ModelAttribute("uploadItem")UploadFileItem uploadItem,
			Model model) {

		//validate
		if(!validateUploadItem(uploadItem, model)){
			model.addAttribute(ITEM_NAME, uploadItem);
			model.addAttribute(TYPES, typeManager.getAll());
			model.addAttribute(Constants.ACTIVE_HEADER, UPLOAD);
			return START_UPLOAD_PAGE;
		}

		//create full path to folder: starting from src/...
		//filePath

		String parentItemFilePath = filePath + File.separator + uploadItem.getName();
		String itemFilePath = parentItemFilePath + File.separator + uploadItem.getFileName();
		File pathToFileDirFile = new File (parentItemFilePath);
		File transferZipFile = new File(itemFilePath);
		String zipHash;

		//save MainFile
		try {
			createFilesAndStoreMultipartFile(pathToFileDirFile, transferZipFile, uploadItem);
			zipHash = FileValidate.createHash(transferZipFile);
		} catch (IOException e) {
            FileValidate.rollbackFolderCreation(itemFilePath);
            logger.error(e.getMessage() +" ††† "+ e.getStackTrace());
			return START_UPLOAD_PAGE;
		} catch (NoSuchAlgorithmException e) {
			FileValidate.rollbackFolderCreation(itemFilePath);
			logger.error(e.getMessage() +" ††† "+ e.getStackTrace());
			return START_UPLOAD_PAGE;
		}

        if(!validateZipHash(zipHash, parentItemFilePath)){
            model.addAttribute(ITEM_NAME, uploadItem);
            model.addAttribute(TYPES, typeManager.getAll());
            model.addAttribute(Constants.ACTIVE_HEADER, UPLOAD);
            model.addAttribute(ZIP_OCCUPIED, true);
            return START_UPLOAD_PAGE;
        }

		//saving fileInfo in base
		FileInfo fileInfo = createFileInfo(uploadItem, zipHash);
		//saving pictures in filesystem and base

		//unzipping
		unzipFileAddPictures(itemFilePath, parentItemFilePath, fileInfo);
		return "redirect:/";
	}

	protected void createFilesAndStoreMultipartFile(File pathToFileDirFile,
													File transferZipFile,
													UploadFileItem uploadItem) throws IOException {
		pathToFileDirFile.mkdirs();
		transferZipFile.createNewFile();
		uploadItem.getFileData().transferTo(transferZipFile);
	}

    boolean validateZipHash(String zipHash, String parentFolder){
        if(fileInfoManager.isHashContains(zipHash)){
            try {
				deleteDirectory(parentFolder);
            } catch (IOException e) {
                logger.error("can't delete folder "+ parentFolder);
            }
            return false;
        }
        return true;
    }

	protected void deleteDirectory(String directory) throws IOException {
		FileUtils.deleteDirectory(new File(directory));
	}


	/**
	 * validating upload file form; if it has validation errors => adds to model attributes for represent in on view
	 * @param uploadItem the item that represents upload form
	 * @param model the current model in this request
	 * @return valid UploadItem object or not
	 */
	boolean validateUploadItem(UploadFileItem uploadItem, Model model){
        boolean valid = true;
        if(uploadItem.getName() == null || uploadItem.getName().isEmpty()){
            valid = false;
            model.addAttribute(NAME_NOT_VALID, true);
        }
        if(uploadItem.getDescription() != null && uploadItem.getDescription().length() > 225){
            valid = false;
            model.addAttribute(DESC_NOT_VALID, true);
        }
        String filename = uploadItem.getFileName();
        if(filename == null || !filename.endsWith(ZIP_EXTENTION)){
            valid = false;
            model.addAttribute(FILE_NOT_VALID, true);
        }
        if(fileInfoManager.isNameOccupied(uploadItem.getName())){
            valid = false;
            model.addAttribute(NAME_OCCUPIED, true);
        }
		if(!typeManager.isTypeByIdContains(uploadItem.getType())){
            valid = false;
            model.addAttribute(TYPE_INVALID, true);
        }
		//here I've added option for valid and not valid files
		if(uploadItem.isValidateFile() && !validateZip(uploadItem.getFileData()) ||
				!uploadItem.getFileName().endsWith(ZIP_EXTENTION)){
			valid = false;
			model.addAttribute(FILE_NOT_VALID, true);
		}
        return valid;
	}

	/**
	 * create and save in database fileInfo object, it's desirable to call the method after 
	 * file save in filesystem
	 * here shouldn't be NFE cause of preValidation
	 * 
	 * @param item the wrap-object of fileItem
	 * @param hash is hash of zip file
	 * @return fileInfo which saved in database
	 */
	FileInfo createFileInfo(UploadFileItem item, String hash){
		FileInfo createdFile = new FileInfo();
		try{
			createdFile.setName(item.getName());
			createdFile.setDescription(item.getDescription());
			createdFile.setFilePath(item.getFileName());
			createdFile.setType(typeManager.getTypeById(Long.parseLong(item.getType())));
			createdFile.setHash(hash);
		} catch (NumberFormatException nfe){
			logger.error("have number parcing error; can't parse " + item.getType() + " as long" + Arrays.toString(nfe.getStackTrace()));
		}

		//TODO: check on get another name
		//TODO: auto-create of data and downloads
		fileInfoManager.saveFileInfo(createdFile);
		
		return createdFile;
	}
	
	void unzipFileAddPictures(final String filePath, final String folderPath, final FileInfo fileInfo){
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					unzipFile(filePath);
                    List<String> picturePathes = getPathesOfPictures(folderPath);
                    String titleImage = findImageWithConcreteSize(folderPath, picturePathes, TITLE_IMAGE_SIZE,
							TITLE_IMAGE_SIZE, ACCURACY);
                    String iconImage = findImageWithConcreteSize(folderPath, picturePathes, ICON_IMAGE_SIZE,
							ICON_IMAGE_SIZE, ACCURACY);

                    fileInfo.setIcon(iconImage);
                    fileInfo.setMainTitle(titleImage);
                    fileInfoManager.saveFileInfo(fileInfo);
				} catch (IOException e) {
					logger.error("can't unzip "+ filePath +
					" cause of \n " + e.getMessage() + "\n" + e.getStackTrace());
				}
			}
		}).run();

	}

	protected void unzipFile(String filePath) throws IOException {
		SaveFileUtils.getInstance().unzipFile(filePath);
	}

	protected List<String> getPathesOfPictures(String folderPath){
		return PictureUtils.getPathesOfPictures(folderPath);
	}

	protected String findImageWithConcreteSize(String pathPrefix,List<String> filePathes,
											   int height, int width, int accuracy){
		return PictureUtils.findImageWithConcreteSize(pathPrefix,filePathes, height, width, accuracy);
	}

	protected boolean validateZip(CommonsMultipartFile file){
		return FileValidate.validateZip(file);
	}



	public void setFileInfoManager(FileInfoManager fileInfoManager) {
		this.fileInfoManager = fileInfoManager;
	}

	public void setTypeManager(TypeManager typeManager) {
		this.typeManager = typeManager;
	}
}
