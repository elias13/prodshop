package com.dataart.productshop.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "type")
public class Type {

	@Id
	@GeneratedValue
	@Column
	private Long id;

	@Column
	private String name;

	@OneToMany(mappedBy = "type")
	private List<FileInfo> fileInfos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FileInfo> getFileInfos() {
		return fileInfos;
	}

	public void setFileInfos(List<FileInfo> fileInfoId) {
		this.fileInfos = fileInfoId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Type)) return false;

		Type type = (Type) o;

		if (fileInfos != null ? !fileInfos.equals(type.fileInfos) : type.fileInfos != null) return false;
		if (id != null ? !id.equals(type.id) : type.id != null) return false;
		if (name != null ? !name.equals(type.name) : type.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (fileInfos != null ? fileInfos.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Type{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
