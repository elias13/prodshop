package com.dataart.productshop.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fileinfo")
public class FileInfo {
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column
	private String description;
	
	@Column(name = "file_path",nullable = false)
	private String filePath;
	
	@Column(nullable = false)
	private Long downloads;
	
	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "hash")
	private String hash;

    @Column(name = "main_title")
    private String mainTitle;

    @Column(name = "icon")
    private String icon;


	@ManyToOne
	@JoinColumn(name = "type_id")
	private Type type;

	public FileInfo(){
		downloads = 0L;
		createdDate = new Date();
	}

	/* getters | setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDownloads() {
		return downloads;
	}

	public void setDownloads(Long downloads) {
		this.downloads = downloads;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public Type getType() {
		return type;
	}


	public void setType(Type type) {
		this.type = type;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof FileInfo)) return false;

		FileInfo fileInfo = (FileInfo) o;

		if (createdDate != null ? !createdDate.equals(fileInfo.createdDate) : fileInfo.createdDate != null)
			return false;
		if (description != null ? !description.equals(fileInfo.description) : fileInfo.description != null)
			return false;
		if (downloads != null ? !downloads.equals(fileInfo.downloads) : fileInfo.downloads != null) return false;
		if (filePath != null ? !filePath.equals(fileInfo.filePath) : fileInfo.filePath != null) return false;
		if (hash != null ? !hash.equals(fileInfo.hash) : fileInfo.hash != null) return false;
		if (icon != null ? !icon.equals(fileInfo.icon) : fileInfo.icon != null) return false;
		if (id != null ? !id.equals(fileInfo.id) : fileInfo.id != null) return false;
		if (mainTitle != null ? !mainTitle.equals(fileInfo.mainTitle) : fileInfo.mainTitle != null) return false;
		if (name != null ? !name.equals(fileInfo.name) : fileInfo.name != null) return false;
		if (type != null ? !type.equals(fileInfo.type) : fileInfo.type != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (filePath != null ? filePath.hashCode() : 0);
		result = 31 * result + (downloads != null ? downloads.hashCode() : 0);
		result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
		result = 31 * result + (hash != null ? hash.hashCode() : 0);
		result = 31 * result + (mainTitle != null ? mainTitle.hashCode() : 0);
		result = 31 * result + (icon != null ? icon.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "FileInfo{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", filePath='" + filePath + '\'' +
				", downloads=" + downloads +
				", createdDate=" + createdDate +
				", hash='" + hash + '\'' +
				", mainTitle='" + mainTitle + '\'' +
				", icon='" + icon + '\'' +
				", type=" + type +
				'}';
	}
}
