package com.dataart.productshop.file.utils;

import static com.dataart.productshop.Constants.EXT_PREFIX;
import static com.dataart.productshop.Constants.EXT_SUFFIX;
import static com.dataart.productshop.Constants.WORD_EXPR;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileValidate {


	private static final Logger logger = LoggerFactory.getLogger(FileValidate.class);
	private static final String ENCODING_NAME = "SHA-256";

	public static boolean validateZip(MultipartFile mFile){
		InputStream is;
		try{
			if(mFile.getInputStream().available() < 1){
				return false;
			}
			is = mFile.getInputStream();
		}catch(IOException ex){
			return false;
		}
		return validateZipFilename(mFile.getOriginalFilename()) && validateConvertZip(is);
	}

	static boolean validateZipFilename(String fileName){
		return fileName.matches(EXT_PREFIX + "zip" + EXT_SUFFIX);
	}

	/**
	 * searching zip input stream for necessary txt file with valide inner content
	 * (name : somename
	 * package:somepackage)
	 * @param zipInputStream
	 * @return
	 */
	public static boolean validateConvertZip(InputStream zipInputStream){
		try {
		
			ZipInputStream zis = new ZipInputStream(zipInputStream);
			byte[] byteBuffer = new byte[1024];

			ZipEntry ze = zis.getNextEntry();
			while(ze != null){
				if(checkWordExpressionForZipFile(ze, zis, byteBuffer)){
					return true;
				}
				ze = zis.getNextEntry();
			}
		} catch (IOException e) {
			logger.error("bad zip stream");
		} finally {
			try {
				zipInputStream.close();
			} catch (IOException e) {
				logger.error(e.getMessage() + e.getStackTrace());
			}
		}
		return false;
	}

	static boolean checkWordExpressionForZipFile(ZipEntry ze, ZipInputStream zis, byte [] byteBuffer) throws IOException {
		String filename = ze.getName();
		if(filename.matches(EXT_PREFIX + "txt" + EXT_SUFFIX)){

			//read once, cause text need to be placed at the start of file
			StringBuilder bcontent = new StringBuilder();
			while(zis.read(byteBuffer) > 0){
				bcontent.append(new String(byteBuffer));
			}

			Pattern validatePat = Pattern.compile(WORD_EXPR);
			Matcher validateMatcher = validatePat.matcher(bcontent.toString());

			if(validateMatcher.find()){
				return true;
			}
		}
		return false;
	}


	public static String createHash(File file) throws IOException, NoSuchAlgorithmException {
		String fileHash;
		MessageDigest digest = MessageDigest.getInstance(ENCODING_NAME);
		InputStream is = new FileInputStream(file);
		byte[] buffer = new byte[8192];
		int read;
		try {
			while( (read = is.read(buffer)) > 0) {
				digest.update(buffer, 0, read);
			}
			byte[] md5sum = digest.digest();
			BigInteger bigInt = new BigInteger(1, md5sum);
			fileHash = bigInt.toString(16);
		}
		finally {
			is.close();
		}
		return fileHash;
	}

	public static void rollbackFolderCreation(String folderpath){
		File fileToDel = new File(folderpath);
		fileToDel.delete();
	}
	
	
}
