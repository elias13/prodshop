package com.dataart.productshop.file.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SaveFileUtils {

	private static final Logger logger = LoggerFactory.getLogger(SaveFileUtils.class);

	private static SaveFileUtils utils;

	protected SaveFileUtils() {
	}

	public static SaveFileUtils getInstance(){
		if(utils == null){
			utils = new SaveFileUtils();
		}
		return utils;
	}


	/**
	 * unzip zip into current folder
	 *
	 * @param inputFileName name of zip
	 * @throws IOException
	 */
	public void unzipFile(String inputFileName) throws IOException {
		byte[] buffer = new byte[1024];

		ZipInputStream zis = new ZipInputStream(new FileInputStream(inputFileName));
		File parent = new File(inputFileName);
		if(!parent.exists()){
			throw new FileNotFoundException("file " + inputFileName + "not contains");
		} else {
			parent = parent.getParentFile();
		}
		try{
			ZipEntry ze = zis.getNextEntry();
			while(ze != null){
				String fileName = ze.getName();
				File newFile = new File(parent.getAbsolutePath() + File.separator + fileName);

				System.out.println("file unzip : "+ newFile.getAbsoluteFile());

				//create all non exists folders
				//else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}
		}finally{
			zis.closeEntry();
			zis.close();
		}

	}


}
