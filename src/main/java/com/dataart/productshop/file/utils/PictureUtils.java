package com.dataart.productshop.file.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dataart.productshop.Constants;

public class PictureUtils {

    private static final Logger logger = LoggerFactory.getLogger(PictureUtils.class);

	/**
     * find first Image from offered that fits the parameters
     * with presented accuracy;
     * also get the closest picture variant to necessary (if there are several fits)
	 * so if we have two pictures with 128x128 size and 127x128 size
	 * and parameters: height:128, width:128, accuracy:5 (any accuracy)
	 * the result would be path with 128x128 picture
     *
     * @param pathPrefix prefix to all file pathes; should be added if necessary
     * @param filePathes pathesOfFile to select from
     * @param height height of final image
     * @param width width of final image
     * @param accuracy represents maximal difference between picture size values and height(width);
     *                 picture always smaller!!
     * @return image path or null if no corresponding image been found
     */
    public static String findImageWithConcreteSize(String pathPrefix,
											List<String> filePathes,
											int height, int width, int accuracy){
        if(height < 0){
            height = 0;
        }
        if(width < 0){
            width = 0;
        }
        if(accuracy < 0){
            accuracy = 0;
        }

        int difference = width + height;
        String currentPath = null;

        for (String filePath : filePathes) {
            try {
                BufferedImage img = ImageIO.read(new File(pathPrefix + "/" + filePath));
                int pHeight = img.getHeight();
                int pWidth = img.getWidth();
                if(pHeight <= height && pWidth <= width
                        && (pWidth >= width - accuracy || pHeight >= height - accuracy)
                        && ((height - pHeight) + (width - pWidth) < difference) ) {
                    currentPath = filePath;
                    difference = (height - pHeight) + (width - pWidth);
                }
            } catch (IOException e) {
                logger.error(filePath + " is not image path");
            }
        }
            return currentPath;
    }

    /**
     * this methods returns the full list of pictures in exact folder
     * @param pathToFolder path to folder to seek
     * @return list of picture pathes(by default find only .jpg .bmp .png files)
     */
    public static List<String> getPathesOfPictures(String pathToFolder){
        File dir = new File(pathToFolder);

        if(dir.isDirectory()==false){
            System.out.println("Directory does not exists : " + pathToFolder);
            return null;
        }

        String [] listOfPictures = dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                for (String ext : Constants.PICTURE_EXT) {
                    if(name.endsWith(ext)){
                        return true;
                    }
                }
                return false;
            }
        });

        return Arrays.asList(listOfPictures);
    }


}
