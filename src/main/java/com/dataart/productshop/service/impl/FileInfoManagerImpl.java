package com.dataart.productshop.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataart.productshop.dao.FileInfoDao;
import com.dataart.productshop.model.FileInfo;
import com.dataart.productshop.service.FileInfoManager;

@Service("fileInfoManager")
@Transactional
public class FileInfoManagerImpl extends BaseManagerImpl<FileInfo> implements FileInfoManager{

	@Autowired
	public void setFileInfoDao(FileInfoDao fileInfoDao) {
		this.fileInfoDao = fileInfoDao;
		setBaseDao(fileInfoDao);
	}

	@Override
	public List<FileInfo> getFirstFive() {
		List<FileInfo> list = fileInfoDao.getByDownloads(0, 5);
		return list != null ? list : new ArrayList<FileInfo>(5);
	}

	@Override
	public List<FileInfo> getFullFive() {
		List<FileInfo> list = getFirstFive();
		while (list.size() < 5){
			list.add(null);
		}
		return list;
	}

	@Override
	public void saveFileInfo(FileInfo input) {
		fileInfoDao.save(input);
	}

	@Override
	public List<FileInfo> getFileInfoByTypeId(Long id) {
		return fileInfoDao.getFileInfoByTypeId(id);
	}

	@Override
	public boolean isNameOccupied(String name) {
		return fileInfoDao.getByName(name) != null;
	}

	@Override
	public FileInfo getByName(String name) {
		return fileInfoDao.getByName(name);
	}

    @Override
    public boolean isHashContains(String hash) {
        return fileInfoDao.getByHash(hash) != null;
    }

}
