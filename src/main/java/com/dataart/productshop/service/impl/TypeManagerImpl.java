package com.dataart.productshop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataart.productshop.dao.TypeDao;
import com.dataart.productshop.model.Type;
import com.dataart.productshop.service.TypeManager;

@Service("typeManager")
@Transactional
public class TypeManagerImpl extends BaseManagerImpl<Type> implements TypeManager{

	private TypeDao typeDao; //TODO: remove. this dao has to be already incapsulated in BaseManagerImpl
	
	@Autowired
	public void setTypeDao(TypeDao typeDao){
		this.typeDao = typeDao;
		setBaseDao(typeDao);
	}
	
	@Override
	public void saveType(Type type) {
		typeDao.save(type);
		
	}

	@Override
	public Type getTypeById(Long id) {
		try{
			return typeDao.getById(id);
		}catch(DataAccessException ex){
			return null;
		}
	}

    @Override
      public Type getByName(String name) {
        return typeDao.getByName(name);
    }

    @Override
    public boolean isTypeContains(String name) {
        return getByName(name) != null;
    }

    @Override
    public boolean isTypeByIdContains(Long id) {
        return getTypeById(id) != null;
    }

    @Override
    public boolean isTypeByIdContains(String id) {
        long idLong;
        try{
            idLong = Long.parseLong(id);
        }catch (NumberFormatException ex){
            return false;
        }
        return getTypeById(idLong) != null;
    }


}
