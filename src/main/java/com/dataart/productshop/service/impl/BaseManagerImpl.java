package com.dataart.productshop.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dataart.productshop.dao.BaseDao;
import com.dataart.productshop.dao.FileInfoDao;
import com.dataart.productshop.dao.TypeDao;
import com.dataart.productshop.service.BaseManager;

@Service
@Transactional
public class BaseManagerImpl<T> implements BaseManager<T>{


	private BaseDao<T> baseDao; //TODO: did you mean private <D extends BaseDao<T>> D dao?

    protected FileInfoDao fileInfoDao;

    protected TypeDao typeDao;

	public BaseManagerImpl(){
		
	}

	@Override
	public T getById(Long id) {
		return baseDao.getById(id);
	}

	@Override
	public void save(T entity) {
		baseDao.save(entity);
		
	}

	@Override
	public List<T> getAll() {
		return baseDao.getAll();
	}

	@Override
	public void delete(Long id) {
		baseDao.delete(id);
		
	}

    public BaseDao<T> getBaseDao() {
        return baseDao;
    }

    public void setBaseDao(BaseDao<T> baseDao) {
        this.baseDao = baseDao;
    }


}
