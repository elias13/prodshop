package com.dataart.productshop.service;

import com.dataart.productshop.model.Type;

public interface TypeManager extends BaseManager<Type>{

	void saveType(Type type);
	
	Type getTypeById(Long id);

	void delete(Long id);

    Type getByName(String name);

    /**
     * check whether type exists
     * @param name type's name
     * @return true id type with such name exists
     */
    boolean isTypeContains(String name);

    /**
     * check whether type exists
     * @param id type's id
     * @return true id type with such id exists
     */
    boolean isTypeByIdContains(Long id);

    /**
     * check whether type exists
     * if it not number = return false
     * @param id type's id = represents in string
     * @return true id type with such id exists; also false if id is not number
     */
    boolean isTypeByIdContains(String id);
	
}