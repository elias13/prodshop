package com.dataart.productshop.service;

import java.util.List;

public interface BaseManager<T> {
	T getById (Long id);
	
	void save (T entity);
	
	List<T> getAll();
	
	void delete (Long id);
}
