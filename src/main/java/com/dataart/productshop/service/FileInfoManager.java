package com.dataart.productshop.service;

import java.util.List;

import com.dataart.productshop.model.FileInfo;

public interface FileInfoManager extends BaseManager<FileInfo>{
	
	FileInfo getById(Long id);

	List<FileInfo> getFirstFive();

	/**
	 * this method returns List<FileInfo> with five elements
	 * ordered by donloads and create time
	 * if total number of FileInfo in database less than five
	 * empty places fill with null
	 * @return list of most popular FileInfo by download; list always 5 element long
	 */
	List<FileInfo> getFullFive();
	
	void saveFileInfo(FileInfo input);
	
    List<FileInfo> getFileInfoByTypeId(Long id);

   	boolean isNameOccupied(String name);

	FileInfo getByName(String name);

	void delete(Long id);

    boolean isHashContains(String hash);
}
