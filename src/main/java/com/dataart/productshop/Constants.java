package com.dataart.productshop;

public interface Constants {

	String PROP_FILE_PATH = "${store.path}";

    String DEFAULT_IMAGE_URL = "${default.image}";

	String EXT_PREFIX ="([^\\s]+(\\.(?i)(";
	
	String EXT_SUFFIX ="))$)";
	
	String WORD_EXPR = "name:\\s*\\w[\\w\\s]*\\n[\\s]*package:\\s*\\w[\\w\\s]+";
	
	/**
	 * filepath without final filename and without backslash 
	 */
    String [] PICTURE_EXT = {".jpg",".png",".bmp",".gif"};
	
	/* ----------------------------Model names ---------------------------- */

		String ACTIVE_HEADER = "active_header";

	/* ----------------------------Main Controller---------------------------- */
}
