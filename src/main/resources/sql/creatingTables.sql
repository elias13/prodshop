
CREATE SCHEMA `store` ;


CREATE  TABLE `store`.`type` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `name` VARCHAR(225) NOT NULL ,

  PRIMARY KEY (`id`) );
  

CREATE  TABLE `store`.`fileinfo` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `name` VARCHAR(225) NOT NULL ,

  -- you could put here more than 225 submols
  -- but than change the static final field UploadController.DESCR_SIZE
  `description` VARCHAR(225),
  
  `file_path` VARCHAR(225) NOT NULL ,

  `downloads` BIGINT NOT NULL DEFAULT 0 ,

  `created_date` DATETIME NOT NULL ,
  
  `type_id` BIGINT NOT NULL,

  `hash` VARCHAR (32),

  `icon` VARCHAR(225),

  `type_id` VARCHAR(225),
  
  PRIMARY KEY (`id`),
  
  FOREIGN KEY (type_id) references type(id));
  
  
CREATE  TABLE `store`.`picture` (

  `id` BIGINT NOT NULL AUTO_INCREMENT ,

  `file_path` VARCHAR(225) NOT NULL ,
  
  `file_info_id` BIGINT NOT NULL,

  PRIMARY KEY (`id`),
  
  FOREIGN KEY (file_info_id) references fileinfo(id));

